const initialDataRender = () => {
    const spentByList = document.querySelector("#spentBy");
    const spentForList = document.querySelector("#spentFor");

    const everyone = JSON.parse(localStorage.getItem("allPerson"));
    everyone.forEach(person=>{
        const element = addNewAuthor(person,true);
        spentByList.appendChild(element);
        const element2 = addNewAuthor(person,false);
        spentForList.appendChild(element2);
    })
}

const addNewAuthor = (personInfo,spentBy=false) => {
    const newElement = document.createElement('div');
    const inp = document.createElement('input');
    if(spentBy) {
        inp.setAttribute('type','radio');
        inp.setAttribute('name','spent');

    } else {
        inp.setAttribute('type','checkbox');
    }
    inp.setAttribute('value',personInfo.name);
    const textNode = document.createTextNode(personInfo.name);
    newElement.appendChild(inp);
    newElement.appendChild(textNode);
    return newElement;
}
const finalBalanceForPersone = (personData) => {
    const newNode = document.createElement('div');
    newNode.setAttribute('class','finalBalanceNode');
    const spanForName = document.createElement('span');
    spanForName.setAttribute('class','balanceName');
    spanForName.textContent = personData.name;
    const spanForAmount = document.createElement('span');
    spanForAmount.setAttribute('class','balanceAmount');
    spanForAmount.textContent = personData.amount;

    newNode.appendChild(spanForName);
    newNode.appendChild(spanForAmount);
    return newNode;
}

const updateFinalBalance = () => {
    const allPerson = JSON.parse(localStorage.getItem("allPerson"));
    const getFinalBalanceContainer = document.getElementById('footer');
    getFinalBalanceContainer.innerHTML = null;
    allPerson.forEach((personData)=>{
        getFinalBalanceContainer.appendChild(finalBalanceForPersone(personData));
    })
}

const AddNewAuthorButtonClicked = (e) => {
    const personInfoContainer = e.target.parentNode;

    const personName = personInfoContainer.querySelector('input').value;
    const initalAmount = 0;
    const newPerson = {
        name:personName,
        amount:initalAmount
    }
    const ListOfAuthors = JSON.parse(localStorage.getItem("allPerson"));
    if(ListOfAuthors) {
        const newList = ListOfAuthors.filter(person => person.name!=personName);
        newList.push(newPerson);
        localStorage.setItem("allPerson",JSON.stringify(newList));
    } else {
        const newList = [];
        newList.push(newPerson);
        localStorage.setItem("allPerson",JSON.stringify(newList));
    }

    const spentByContainer = document.querySelector("#spentBy");
    const newElementSpendBy = addNewAuthor(newPerson,true);

    const spentFor = document.querySelector('#spentFor');
    const newElementSpentFor = addNewAuthor(newPerson);
    
    spentByContainer.appendChild(newElementSpendBy);
    spentFor.appendChild(newElementSpentFor);
}
const updateExpensesContainer = (spentAmount,allSpendees, paidBy) => {

    const newElement = document.createElement('div');
    newElement.setAttribute('class','expense');
    const amountDiv = document.createElement('div');
    amountDiv.textContent = `Amount Spent: ${spentAmount}`;
    const spentByDiv = document.createElement('div');
    spentByDiv.textContent =`Paid by: ${paidBy}`;
    const spendeeListDic = document.createElement('div');
    spendeeListDic.setAttribute('class','expenseSpendeeList');
    const headingForSpendee = document.createElement('span');
    headingForSpendee.textContent = "Divided between (including the spendee)";
    spendeeListDic.appendChild(headingForSpendee);

   
    const allSpendeeList = Object.keys(allSpendees);
    allSpendeeList.forEach(person=> {
        const el = document.createElement('div');
        el.setAttribute('class','spendeeName');
        el.textContent = person;
        spendeeListDic.appendChild(el);
    });

    newElement.appendChild(amountDiv);
    newElement.appendChild(spentByDiv);
    newElement.appendChild(spendeeListDic);

    const allExpensedConatainer = document.querySelector(".contentContainer");
    allExpensedConatainer.appendChild(newElement);

}
const AddExpenseButtonClicked = (e) => {
    const containerExpense = e.target.parentNode.parentNode;
    const spentByList = containerExpense.querySelector("#spentBy").querySelectorAll('input');
    const spentForList = containerExpense.querySelector("#spentFor").querySelectorAll('input');
    const spentAmount = parseInt(containerExpense.querySelector('#spentAmount').value);
    let spenderName,numberOfParticipant=0;
    spentByList.forEach((person)=>{
        if(person.checked) {
            spenderName = person.value;
        }
    })
    const allSpendees = {};
    spentForList.forEach(inp => {
        if(inp.checked) {
            const newMember = inp.value;
            allSpendees[newMember] = 1;
            numberOfParticipant++;
        } 
    });
    const share = Math.ceil(spentAmount/numberOfParticipant);

    const allPersoneList = JSON.parse(localStorage.getItem("allPerson"));
    const newList = [];
    allPersoneList.forEach(per=>{
        const perName = per.name;
        const newItem = {};
        if(allSpendees[perName]) {
            newItem.name = perName;
            newItem.amount = per.amount + share;
            if(perName==spenderName) {
                newItem.amount -= spentAmount;
            }
            newList.push(newItem);
        } else {
            newList.push(per);
        }
    });

    localStorage.setItem("allPerson",JSON.stringify(newList));
    updateExpensesContainer(spentAmount,allSpendees,spenderName );
    updateFinalBalance();
}


initialDataRender();